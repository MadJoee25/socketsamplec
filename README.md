# README #

Esempio creazione socket in C

### Non riesci a compilare? ###

Ricordati di disabilitare le estensioni di linguaggio prima di compilare!
https://docs.microsoft.com/it-it/cpp/build/reference/za-ze-disable-language-extensions?view=msvc-170

### Come si utilizza? ###

Per avviare il server una volta compilato digitate nella console:
Windows: .\server.exe porta Unix: ./server porta
Esempio: .\server.exe 27015

Per avviare il client una volta compilato digitate nella console:
Windows: .\client.exe porta Unix: ./client porta
Esempio: .\client.exe 27015

Ricorda che le porte tra client e server devono combaciare!

### Non riesci ad associare la socket? ###

Cambia porta! Molto spesso per qualche ragione quella porta non consente l'accesso.
Ricorda anche di disabilitare il firewall per evitare ulteriori problemi!